
package com.company;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        AccountsReader reader = new AccountsReader();
        List<Account> accountList = reader.readAccounts();
        BankManager bankManager = BankManager.getINSTANCE();
        bankManager.openBank(accountList);
    }
}
