package com.company;

import java.io.*;
import java.util.List;
import java.util.Scanner;

class BankManager {

    private static boolean isLoggedIn = false;
    private static BankManager INSTANCE = null;
    private List<Account> accountList;
    private int currentAccountIndex;

    static BankManager getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new BankManager();
        }
        return INSTANCE;
    }

    void openBank(List<Account> accountList) {
        this.accountList = accountList;
        displayMenu();
    }

    private void displayMenu() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Display Menu");
        System.out.println("1. Login");
        System.out.println("2. Logout");
        System.out.println("3. AccountCreation");
        System.out.println("4. Inspect Account");
        if (isLoggedIn) {
            System.out.println("5. Make a transfer");
        }
        System.out.println("6. Exit");
        System.out.print("Enter your option: ");
        String choice = scan.nextLine();
        switch (choice) {
            case "1":
                login();
                break;
            case "2":
                logout();
                break;
            case "3":
                createAccount();
                break;
            case "4":
                inspectAccount();
                break;
            case "5":
                paymentMethod();
                break;
            case "6":
                System.exit(0);
            default:
                System.out.println("Choose a valid option!");
                System.out.println();
                displayMenu();
                break;
        }
    }

    private void paymentMethod() {
        System.out.print("Choose the account number where you want to transfer the money: ");
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        System.out.print("Enter the sum for transfer: ");
        String moneyRaw = scanner.nextLine();
        int money = Integer.parseInt(moneyRaw);//to do validation
        if (money > accountList.get(currentAccountIndex).getSold()) {
            System.out.println("Invalid operation!Not enough money!");
            displayMenu();
        } else {
            transferMoneyFromCurrentAccount(number, money);
        }
    }

    private void transferMoneyFromCurrentAccount(String number, int money) {
        for (Account account : accountList) {
            if (account.getNumber().equals(number)) {
                if (account.getCurrency().equals(accountList.get(currentAccountIndex).getCurrency())) {
                    account.addMoney(money);
                    accountList.get(currentAccountIndex).withdrawMoney(money);
                    updateFile();
                    System.out.println("Transfer succeeded!!");
                } else {
                    System.out.println("Choose same currency type account!");
                }
            }
        }
        System.out.println();
        displayMenu();
    }

    private void updateFile() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("resources/file/accounts_details.txt"));
            for (Account account : accountList) {
                writer.write(account.toString());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void inspectAccount() {
        BufferedReader inputStream = null;
        try {
            inputStream = new BufferedReader(new FileReader("resources/file/accounts_details.txt"));
            String count;
            StringBuilder stringBuilder = new StringBuilder();
            while ((count = inputStream.readLine()) != null) {
                stringBuilder.append(count).append("\n");
            }
            System.out.println(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        displayMenu();
    }

    private void createAccount() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your account number:");
        String number = scanner.nextLine();
        System.out.print("Enter your account username:");
        String username = scanner.nextLine();
        System.out.print("Enter your account password:");
        String password = scanner.nextLine();
        System.out.print("Enter your account balance:");
        String balance = scanner.nextLine();
        System.out.print("Enter your account type:");
        String type = scanner.nextLine();

        Account newAccount = new Account(number, username, password, Integer.parseInt(balance), type);
        accountList.add(newAccount);

        updateFile();
        System.out.println("Successfully created! ");
        System.out.println();
        displayMenu();
    }

    private void logout() {
        System.out.println("Successfully logout!");
        System.out.println();
        isLoggedIn = false;
        displayMenu();
    }

    private void login() {
        if (isLoggedIn) {
            System.out.println("You are already logged in. Please log out first.");
            System.out.println();
            displayMenu();
            return;
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter username: ");
        String username = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();
        boolean found = false;
        for (Account account : accountList) {
            if (account.getUsername().equals(username) && account.getPassword().equals(password)) {
                System.out.println("Welcome username");
                found = true;
                currentAccountIndex = accountList.indexOf(account);
                isLoggedIn = true;
                break;
            }
        }
        if (!found) {
            System.out.println("Wrong username/password");
        }
        System.out.println();
        displayMenu();
    }
}