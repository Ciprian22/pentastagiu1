package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class AccountsReader {

    List<Account> readAccounts() {
        List<Account> accountList = new ArrayList<>();
        try {
            BufferedReader inputStream = new BufferedReader(new FileReader("resources/file/accounts_details.txt"));

            String line;
            while ((line = inputStream.readLine()) != null) {

                String[] words = line.split(" ");
                Account account = new Account(words[0], words[1], words[2], Integer.parseInt(words[3]), words[4]);
                accountList.add(account);
            }
            inputStream.close();
            return accountList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return accountList;
    }
}
