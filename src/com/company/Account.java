package com.company;

class Account {
    private String number;
    private final String username;
    private final String password;
    private int sold;
    private String currency;

    Account(String number, String username, String password, int sold, String currency) {
        this.number = number;
        this.username = username;
        this.password = password;
        this.sold = sold;
        this.currency = currency;
    }

    String getPassword() {
        return password;
    }

    String getUsername() {
        return username;
    }

    int getSold() {
        return sold;
    }

    String getCurrency() {
        return currency;
    }

    String getNumber() {
        return number;
    }

    void addMoney(int money) {
        this.sold = this.sold + money;
    }

    void withdrawMoney(int money) {
        this.sold = this.sold - money;
    }

    @Override
    public String toString() {
        return number + " " + username + " " + password + " " + sold + " " + currency;
    }
}